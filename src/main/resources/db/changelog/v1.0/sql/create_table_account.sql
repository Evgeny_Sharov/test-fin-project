CREATE TABLE IF NOT EXISTS account
(
    account_id bigint AUTO_INCREMENT PRIMARY KEY,
    account_no bigint UNIQUE NOT NULL,
    person_id  bigint        NOT NULL REFERENCES person,
    pin_code   varchar(255)  NOT NULL,
    balance    numeric(10, 2) default 0.00
        CHECK ( balance >= 0.00 )
);

COMMENT ON TABLE account IS 'Информация о счетах пользователей';
COMMENT ON COLUMN account.account_id IS 'Идентификатор счета';
COMMENT ON COLUMN account.account_no IS 'Номер счета';
COMMENT ON COLUMN account.person_id IS 'Идентификатор пользователя, к которому относится данный счет';
COMMENT ON COLUMN account.pin_code IS 'Пин код к счеты в виде хэша md5';
COMMENT ON COLUMN account.balance IS 'Баланс счета';