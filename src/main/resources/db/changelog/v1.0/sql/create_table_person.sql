CREATE TABLE IF NOT EXISTS person
(
    person_id bigint AUTO_INCREMENT PRIMARY KEY,
    login     varchar(50) UNIQUE NOT NULL
);

COMMENT ON TABLE person IS 'Информация о пользователе';
COMMENT ON COLUMN person.person_id IS 'Идентификатор пользователя';
COMMENT ON COLUMN person.login IS 'Логин пользователя';