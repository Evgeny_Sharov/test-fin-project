CREATE TABLE IF NOT EXISTS transactions
(
    transaction_id bigint AUTO_INCREMENT PRIMARY KEY,
    account_id     bigint REFERENCES account NOT NULL,
    price          numeric(10, 2)            NOT NULL,
    create_dttm    TIMESTAMP WITH TIME ZONE  NOT NULL
);

COMMENT ON TABLE transactions IS 'Информация о транзакциях по всем счетам';
COMMENT ON COLUMN transactions.transaction_id IS 'Идентификатор транзакции';
COMMENT ON COLUMN transactions.account_id IS 'Идентификатор счета по которому проведена данная транзакция';
COMMENT ON COLUMN transactions.price IS 'Стоимость транзакции';
COMMENT ON COLUMN transactions.create_dttm IS 'Время совершения транзакции';