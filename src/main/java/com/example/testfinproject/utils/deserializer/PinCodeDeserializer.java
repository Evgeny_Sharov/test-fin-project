package com.example.testfinproject.utils.deserializer;

import com.example.testfinproject.exceptions.IllegalArgumentException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Десериализатор для проверки корректности пин  кода.
 */
public class PinCodeDeserializer extends JsonDeserializer<String> {

    /**
     * Регулярное выражение на 4 цифры .
     */
    private static final String FOUR_DIGITS = "^(\\d{4})$";

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String pinCode = jsonParser.getValueAsString();
        validatePinCode(pinCode);

        return pinCode;
    }

    private void validatePinCode(String pinCode) {
        if (!pinCode.matches(FOUR_DIGITS)) {
            throw new IllegalArgumentException(String.format("Некорректный формат пин кода: %s. Пинкод должен содержать только 4 цифры", pinCode));
        }
    }
}