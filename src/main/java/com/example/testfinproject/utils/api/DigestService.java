package com.example.testfinproject.utils.api;

/**
 * API по хэшированию данных.
 */
public interface DigestService {

    /**
     *  Хэшировать данные.
     *
     * @param string данные для хэширования.
     * @return даные после хэширования
     */
    String hash(String string);
}