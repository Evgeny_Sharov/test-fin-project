package com.example.testfinproject.service.dto;

import com.example.testfinproject.utils.deserializer.PinCodeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Сервисная модель запроса на перевод денежных средств между счетами.
 */
@Data
@NoArgsConstructor
public class TransferMoneyDto {

    /**
     * Данные о владельце счета с которого выполняется перевод.
     */
    @Valid
    @NonNull
    private PersonDto personDto;

    /**
     * Пин код к счету с которого выполняется перевод.
     */
    @NonNull
    @JsonDeserialize(using = PinCodeDeserializer.class)
    private String pinCode;

    /**
     * Номер счета с которого выполняется перевод.
     */
    @NonNull
    private Long accountNoFrom;

    /**
     * Номер счета для пополнения баланса.
     */
    @NonNull
    private Long accountNoTo;

    /**
     * Сумма перевода.
     */
    @NonNull
    @Min(value = 0)
    private BigDecimal price;
}