package com.example.testfinproject.service.dto;

import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Сервисная модель запроса на пополнение счета.
 */
@Data
@NoArgsConstructor
public class DepositMoneyDto {

    /**
     * Номер счета.
     */
    @NonNull
    private Long accountNo;

    /**
     * Сумма пополнения.
     */
    @NonNull
    @Min(value = 0)
    private BigDecimal price;
}