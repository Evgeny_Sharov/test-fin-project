package com.example.testfinproject.service.api;

import com.example.testfinproject.service.dto.PersonDto;

/**
 * API по работе с пользователями.
 */
public interface PersonService {

    /**
     * Создать нового пользователя.
     *
     * @param personDto модель содержашие данные о новом пользователе.
     * @return
     */
    PersonDto save(PersonDto personDto);

    /**
     * Получить пользователя по его логину.
     *
     * @param personLogin логин пользователя.
     * @return информация о пользователе.
     */
    PersonDto findByLogin(String personLogin);

}