package com.example.testfinproject.service.api;

import com.example.testfinproject.service.dto.DepositMoneyDto;
import com.example.testfinproject.service.dto.AccountDto;
import com.example.testfinproject.service.dto.WithdrawMoneyDto;
import com.example.testfinproject.service.dto.ModelForDisplayingAllAccountsDto;
import com.example.testfinproject.service.dto.TransferMoneyDto;

import java.util.List;

/**
 * API по работе с счетами пользователй.
 */
public interface AccountService {

    /**
     * Получить счета всех пользователей.
     *
     * @return список счетов всех пользователей.
     */
    List<ModelForDisplayingAllAccountsDto> findAll();

    /**
     * Создать новый счет.
     *
     * @param dto Данные для создания счета.
     * @return номер созданного счета.
     */
    String save(AccountDto dto);

    /**
     * Снять деньги со счета.
     *
     * @param dto Данные для снятия с счета.
     * @return Остаток на счете после снятия.
     */
    String withdrawMoney(WithdrawMoneyDto dto);

    /**
     * Пополнить счет.
     *
     * @param dto Данные для пополнения счета (номер счета и сумма).
     * @return Остаток на счете после пополнения.
     */
    String depositMoney(DepositMoneyDto dto);

    /**
     * Перевести деньги на другой счет.
     *
     * @param dto Модель с данными для перевода средств с одного счета на другой.
     * @return Остаток на счете, с оторого выполнялся перевод.
     */
    String transferMoney(TransferMoneyDto dto);

    /**
     * Найти счет по его номеру.
     *
     * @param accountNo Номер счета.
     * @return Данные о счете.
     */
    AccountDto findByAccountNo(Long accountNo);

    /**
     * Обновить баланс счета по его номеру.
     *
     * @param dto номер счета и баланс для обновления в БД.
     */
    void updateBalance(AccountDto dto);
}