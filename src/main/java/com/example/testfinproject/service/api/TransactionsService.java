package com.example.testfinproject.service.api;

import com.example.testfinproject.service.dto.TransactionsDto;

import java.util.List;

/**
 * API по работе с транзакциями.
 */
public interface TransactionsService {

    /**
     * Сохранить транзакцию.
     *
     * @param dto данные о транзакции.
     * @return данные сохраненой транзакции.
     */
    TransactionsDto save(TransactionsDto dto);

    /**
     * Получить транзакции по логину пользователя.
     *
     * @param login логин пользователя.
     * @return список транзакции.
     */
    List<TransactionsDto> findByUserLogin(String login);
}