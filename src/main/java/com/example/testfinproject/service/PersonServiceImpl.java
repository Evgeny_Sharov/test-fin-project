package com.example.testfinproject.service;

import com.example.testfinproject.dao.PersonRepository;
import com.example.testfinproject.dao.entity.PersonEntity;
import com.example.testfinproject.exceptions.NotFoundException;
import com.example.testfinproject.service.api.PersonService;
import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.utils.ModelMapperUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Реализация сервиса по работе с пользователеями.
 */
@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public PersonDto save(PersonDto personDto) {
        PersonEntity personEntity = ModelMapperUtils.map(personDto, PersonEntity.class);
        personRepository.save(personEntity);
        return ModelMapperUtils.map(personEntity, PersonDto.class);
    }

    @Override
    public PersonDto findByLogin(String personLogin) {
        return personRepository.findByLogin(personLogin)
                .map(entity -> ModelMapperUtils.map(entity, PersonDto.class))
                .orElseThrow(() -> new NotFoundException(
                        String.format("Пользователь с идентификатором %s не найден!", personLogin)));
    }
}