package com.example.testfinproject.dao;

import com.example.testfinproject.dao.entity.AccountEntity;
import com.example.testfinproject.dao.entity.ModelForDisplayingAllAccountsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Репозиторий по работе с таблицей account.
 */
@Mapper
@Repository
public interface AccountRepository {

    /**
     * @param accountId
     * @return
     */
    Optional<AccountEntity> findById(@Param("id") Long accountId);

    /**
     * @param accountNo
     * @return
     */
    Optional<AccountEntity> findByAccountNo(@Param("number") Long accountNo);

    /**
     * Получить все счета пользователей.
     *
     * @return список счетов пользоваетлей.
     */
    List<ModelForDisplayingAllAccountsEntity> findAll();


    /**
     * @param entity
     */
    void save(@Param("entity") AccountEntity entity);

    /**
     * Обновить баланс по номеру счета.
     */
    void updateBalance(@Param("entity") AccountEntity entity);
}