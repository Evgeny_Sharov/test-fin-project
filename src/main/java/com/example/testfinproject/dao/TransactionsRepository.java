package com.example.testfinproject.dao;

import com.example.testfinproject.dao.entity.TransactionsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий по работе с таблицей transactions.
 */
@Mapper
@Repository
public interface TransactionsRepository {

    /**
     * Получить все транзакции по логину пользователя.
     *
     * @return
     */
    List<TransactionsEntity> findByPersonLogin(@Param("login") String loginPerson);

    /**
     * @param transactionsEntity
     */
    void save(@Param("entity") TransactionsEntity transactionsEntity);
}