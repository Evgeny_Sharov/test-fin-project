package com.example.testfinproject.dao.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Модель описсывая сущность таблицы account.
 */
@Data
public class AccountEntity {

    /**
     * Идентификатор счета.
     */
    private Long accountId;

    /**
     * Номер счета.
     */
    private Long accountNo;

    /**
     * Идентификатор пользователя.
     */
    private Long personId;

    /**
     * Пин код счета в виде хэша.
     */
    private String pinCode;

    /**
     * Баланс счета.
     */
    private BigDecimal balance;
}