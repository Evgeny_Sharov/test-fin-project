package com.example.testfinproject.dao.entity;

import lombok.Data;

/**
 * Модель описывая сущность таблицы person.
 */
@Data
public class PersonEntity {

    /**
     * Идентификатор пользователя.
     */
    private Long personId;

    /**
     * Логин пользователя.
     */
    private String login;
}