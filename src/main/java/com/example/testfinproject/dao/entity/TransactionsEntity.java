package com.example.testfinproject.dao.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * Модель описывающая сущность таблицы - transactions.
 */
@Data
public class TransactionsEntity {

    /**
     * Идентификатор транзакции.
     */
    private Long transactionId;

    /**
     * Идентификатор счета по которому произошла транзакция.
     */
    private Long accountId;

    /**
     * Сумма транзакции.
     */
    private BigDecimal price;

    /**
     * Время создания транзакции.
     */
    private OffsetDateTime createDttm;
}