package com.example.testfinproject.dao.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Модель для отображения всех счетов пользователй из БД.
 */
@Data
public class ModelForDisplayingAllAccountsEntity {

    /**
     * Логин пользователя.
     */
    private String login;

    /**
     * Номер счета.
     */
    private String accountNo;

    /**
     * Баланс счета.
     */
    private BigDecimal balance;
}