package com.example.testfinproject.dao;

import com.example.testfinproject.dao.entity.PersonEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Репоситорий по работе с БД таблицей person.
 */
@Mapper
@Repository
public interface PersonRepository {

    /**
     * Получить пользователя по его логину.
     *
     * @param personLogin логин пользователя.
     * @return опциональная сущность с информацие о пользователе.
     */
    Optional<PersonEntity> findByLogin(@Param("login") String personLogin);

    /**
     * Сохранить данные о пользователе.
     *
     * @param entity данные о пользователе.
     */
    void save(@Param("entity") PersonEntity entity);
}