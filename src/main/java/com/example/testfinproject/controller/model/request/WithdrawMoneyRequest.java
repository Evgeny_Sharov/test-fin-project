package com.example.testfinproject.controller.model.request;

import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.utils.deserializer.PinCodeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Модель запроса на снятие денежных средств.
 */
@Data
@NoArgsConstructor
@Schema(description = "Модель запроса на снятие денежных средств.")
public class WithdrawMoneyRequest {

    /**
     * Данные о владельце счета.
     */
    @Valid
    @NonNull
    @Schema(description = "Данные о владельце счета.")
    private PersonDto personDto;

    /**
     * Пин код к счету.
     */
    @NonNull
    @Schema(description = "Пин код к счету.")
    @JsonDeserialize(using = PinCodeDeserializer.class)
    private String pinCode;

    /**
     * Номер счета.
     */
    @NonNull
    @Schema(description = "Номер счета.")
    private Long accountNo;

    /**
     * Сумма снятия со счета.
     */
    @NonNull
    @Min(value = 0)
    @Schema(description = "Сумма снятия со счета.")
    private BigDecimal price;
}