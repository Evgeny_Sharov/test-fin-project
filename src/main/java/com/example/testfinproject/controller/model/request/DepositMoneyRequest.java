package com.example.testfinproject.controller.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Модель запроса на пополнение счета.
 */
@Data
@NoArgsConstructor
@Schema(description = "Модель запроса на пополнение счета.")
public class DepositMoneyRequest {

    /**
     * Номер счета.
     */
    @NonNull
    @Schema(description = "Номер счета.")
    private Long accountNo;

    /**
     * Сумма пополнения.
     */
    @NonNull
    @Min(value = 0)
    @Schema(description = "Сумма пополнения.")
    private BigDecimal price;
}