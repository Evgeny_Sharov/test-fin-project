package com.example.testfinproject.controller.model.request;

import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.utils.deserializer.PinCodeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

/**
 * Модель запроса на создание нового счета.
 */
@Data
@NoArgsConstructor
@Schema(description = "Модель запроса на создание нового счета.")
public class CreateAccountRequest {

    /**
     * Данные о владельце счета.
     */
    @Valid
    @NonNull
    @Schema(description = "Данные о владельце счета.")
    private PersonDto personDto;

    /**
     * Пин код счета.
     */
    @NotBlank
    @Schema(description = " Пин код счета.")
    @JsonDeserialize(using = PinCodeDeserializer.class)
    private String pinCode;
}