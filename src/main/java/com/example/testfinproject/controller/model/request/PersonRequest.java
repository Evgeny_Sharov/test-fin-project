package com.example.testfinproject.controller.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Модель запроса на создание нового пользователя.
 */
@Data
@NoArgsConstructor
@Schema(description = "Модель на создание нового пользователя.")
public class PersonRequest {

    /**
     * Логин пользователя.
     */
    @NotBlank
    @Schema(description = "Логин пользователя")
    private String login;
}