package com.example.testfinproject.controller.model.request;

import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.utils.deserializer.PinCodeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Модель запроса на перевод денежных средств между счетами.
 */
@Data
@NoArgsConstructor
@Schema(description = " Модель запроса на перевод денежных средств между счетами.")
public class TransferMoneyRequest {

    /**
     * Данные о владельце счета с которого выполняется перевод.
     */
    @Valid
    @NonNull
    @Schema(description = "Данные о владельце счета с которого выполняется перевод.")
    private PersonDto personDto;

    /**
     * Пин код к счету с которого выполняется перевод.
     */
    @NonNull
    @JsonDeserialize(using = PinCodeDeserializer.class)
    @Schema(description = "Пин код к счету с которого выполняется перевод.")
    private String pinCode;

    /**
     * Номер счета с которого выполняется перевод.
     */
    @NonNull
    @Schema(description = "Номер счета с которого выполняется перевод.")
    private Long accountNoFrom;

    /**
     * Номер счета для пополнения баланса.
     */
    @NonNull
    @Schema(description = "Номер счета для пополнения баланса.")
    private Long accountNoTo;

    /**
     * Сумма перевода.
     */
    @NonNull
    @Min(value = 0)
    @Schema(description = "Сумма перевода.")
    private BigDecimal price;
}