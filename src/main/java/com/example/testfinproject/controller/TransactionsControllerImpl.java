package com.example.testfinproject.controller;

import com.example.testfinproject.controller.api.TransactionsController;
import com.example.testfinproject.service.api.TransactionsService;
import com.example.testfinproject.service.dto.TransactionsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Контроллер по работе с транзакциями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("transactions")
public class TransactionsControllerImpl  implements TransactionsController {

    private final TransactionsService transactionsService;

    @Override
    @GetMapping("login/{login}")
    public List<TransactionsDto> findByUserLogin(@PathVariable @NonNull String login) {
        return transactionsService.findByUserLogin(login);
    }
}