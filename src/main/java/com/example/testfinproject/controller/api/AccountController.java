package com.example.testfinproject.controller.api;

import com.example.testfinproject.controller.model.request.CreateAccountRequest;
import com.example.testfinproject.controller.model.request.DepositMoneyRequest;
import com.example.testfinproject.controller.model.request.TransferMoneyRequest;
import com.example.testfinproject.controller.model.request.WithdrawMoneyRequest;
import com.example.testfinproject.service.dto.ModelForDisplayingAllAccountsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * API контроллера по работе с счетами пользователей.
 */
@Tag(name = " Контроллер по работе с счетами пользователей.")
public interface AccountController {

    @Operation(summary = "Создать новый счет")
    ResponseEntity<String> create(CreateAccountRequest request);

    @Operation(summary = "Получить все счета пользователй")
    List<ModelForDisplayingAllAccountsDto> getAll();

    @Operation(summary = "Сняьть деньги со счета")
    ResponseEntity<String> withdrawMoney(WithdrawMoneyRequest request);

    @Operation(summary = "Внести деньги на счет")
    ResponseEntity<String> depositMoney(DepositMoneyRequest request);

    @Operation(summary = "Первести деньги на другой счет")
    ResponseEntity<String> transferMoney(TransferMoneyRequest request);
}
