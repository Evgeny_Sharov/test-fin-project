package com.example.testfinproject.controller.api;

import com.example.testfinproject.controller.model.request.PersonRequest;
import com.example.testfinproject.service.dto.PersonDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

/*
    Cоздал интерфейс для контроллера, чтобы вынести аннотации для сваггера в интерфейс.
    Т.к может потребоваться более детальная настройка сваггера и чтоб не заграмождать контроллер излишними аннотациями,
     решил их вынести в интерфейс.
 */

/**
 * API контроллера по работе с пользователями.
 */
@Tag(name = "Контроллер по работе с пользователями")
public interface PersonController {

    @Operation(summary = "Добавить нового пользователя")
    PersonDto createUser(PersonRequest request);

    @Operation(summary = "Найти пользователя по логину")
    PersonDto getByLogin(String login);
}