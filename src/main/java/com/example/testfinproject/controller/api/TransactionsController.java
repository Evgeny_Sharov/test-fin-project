package com.example.testfinproject.controller.api;

import com.example.testfinproject.service.dto.TransactionsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/*
    Cоздал интерфейс для контроллера, чтобы вынести аннотации для сваггера в интерфейс.
    Т.к может потребоваться более детальная настройка сваггера и чтоб не заграмождать контроллер излишними аннотациями,
     решил их вынести в интерфейс.
 */

/**
 * API  контроллера по работе с транзакциями.
 */
@Tag(name = "Контроллер по работе с транзакциями.")
public interface TransactionsController {

    @Operation(summary = "Получить все транзакции по логину пользователя", description = "Дескрипшен")
    List<TransactionsDto> findByUserLogin(String login);
}