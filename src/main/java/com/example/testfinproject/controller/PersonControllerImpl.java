package com.example.testfinproject.controller;

import com.example.testfinproject.controller.api.PersonController;
import com.example.testfinproject.controller.model.request.PersonRequest;
import com.example.testfinproject.service.api.PersonService;
import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.utils.ModelMapperUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер по работе с пользователями.
 */
@RestController
@RequestMapping("person")
@RequiredArgsConstructor
public class PersonControllerImpl implements PersonController {

    private final PersonService personService;

    @Override
    @PostMapping("create")
    public PersonDto createUser(@RequestBody PersonRequest request) {
        return personService.save(ModelMapperUtils.map(request, PersonDto.class));
    }

    @Override
    @GetMapping("{login}")
    public PersonDto getByLogin(@PathVariable String login) {
        return personService.findByLogin(login);
    }
}