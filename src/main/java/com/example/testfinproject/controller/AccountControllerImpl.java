package com.example.testfinproject.controller;

import com.example.testfinproject.controller.api.AccountController;
import com.example.testfinproject.controller.model.request.DepositMoneyRequest;
import com.example.testfinproject.controller.model.request.WithdrawMoneyRequest;
import com.example.testfinproject.controller.model.request.CreateAccountRequest;
import com.example.testfinproject.controller.model.request.TransferMoneyRequest;
import com.example.testfinproject.service.api.AccountService;
import com.example.testfinproject.service.dto.DepositMoneyDto;
import com.example.testfinproject.service.dto.AccountDto;
import com.example.testfinproject.service.dto.WithdrawMoneyDto;
import com.example.testfinproject.service.dto.ModelForDisplayingAllAccountsDto;
import com.example.testfinproject.service.dto.TransferMoneyDto;
import com.example.testfinproject.utils.ModelMapperUtils;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Контроллер по работе с счетами пользователей.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("account")
public class AccountControllerImpl implements AccountController {

    private final AccountService accountService;

    @Override
    @PostMapping("create")
    public ResponseEntity<String> create(@RequestBody @Valid CreateAccountRequest request) {
        String accountNo = accountService.save(ModelMapperUtils.map(request, AccountDto.class));
        return ResponseEntity.ok("Создан счет с номером " + accountNo);
    }

    @Override
    @GetMapping("get-all")
    public List<ModelForDisplayingAllAccountsDto> getAll() {
        return accountService.findAll();
    }

    @Override
    @PostMapping("withdraw-money")
    public ResponseEntity<String> withdrawMoney(@RequestBody @Valid WithdrawMoneyRequest request) {
        String balance = accountService.withdrawMoney(ModelMapperUtils.map(request, WithdrawMoneyDto.class));
        return ResponseEntity.ok("Операция по снятию денежных средств выполнена успешна. Остаток на счете: " + balance);
    }

    @Override
    @PostMapping("deposit-money")
    public ResponseEntity<String> depositMoney(@RequestBody @Valid DepositMoneyRequest request) {
        String balance = accountService.depositMoney(ModelMapperUtils.map(request, DepositMoneyDto.class));
        return ResponseEntity.ok("Операция по пополнению счета выполнена успешна. Остаток на счете: " + balance);
    }

    @Override
    @PostMapping("transfer-money")
    public ResponseEntity<String> transferMoney(@RequestBody @Valid TransferMoneyRequest request) {
        String balance = accountService.transferMoney(ModelMapperUtils.map(request, TransferMoneyDto.class));
        return ResponseEntity.ok("Операция по переводу денежных средств выполнена успешна. Остаток на счете: " + balance);
    }
}