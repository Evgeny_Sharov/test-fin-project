package com.example.testfinproject.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Исключение для выбрасывания в случаее некооректных переданных аргументов.
 * Возвращает статус 400.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalArgumentException extends ApplicationException {
    public IllegalArgumentException() {
        super();
    }

    public IllegalArgumentException(String message) {
        super(message);
    }

    public IllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }
}