package com.example.testfinproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Сервис запуска приложения.
 */
@SpringBootApplication
public class TestFinProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestFinProjectApplication.class, args);
    }
}