package com.example.testfinproject.service;

import com.example.testfinproject.dao.PersonRepository;
import com.example.testfinproject.dao.entity.PersonEntity;
import com.example.testfinproject.exceptions.NotFoundException;
import com.example.testfinproject.service.dto.PersonDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.internal.util.Assert;

import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceImplTest {

    @Mock
    private PersonRepository personRepository;

    private PersonServiceImpl personService;

    @Before
    public void setUp() {
        personService = new PersonServiceImpl(personRepository);
    }

    @Test
    public void save() {
        PersonEntity entity = new PersonEntity();
        entity.setLogin("test");

        PersonDto personDto = new PersonDto();
        personDto.setLogin("test");
        PersonDto save = personService.save(personDto);

        verify(personRepository, times(1)).save(entity);
        Assert.notNull(save);
    }

    @Test
    public void findByLogin_ok() {
        PersonEntity entity = new PersonEntity();
        entity.setPersonId(1L);
        entity.setLogin("test");

        doReturn(Optional.of(entity)).when(personRepository).findByLogin("test");

        PersonDto savedDto = personService.findByLogin("test");

        verify(personRepository, times(1)).findByLogin("test");
        Assert.notNull(savedDto);
    }

    @Test(expected = NotFoundException.class)
    public void findByLogin_notFound() {
        PersonEntity entity = new PersonEntity();
        entity.setPersonId(1L);
        entity.setLogin("test");

        doReturn(Optional.empty()).when(personRepository).findByLogin("test");

        personService.findByLogin("test");
    }
}