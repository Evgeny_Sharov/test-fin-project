package com.example.testfinproject.service;

import com.example.testfinproject.dao.TransactionsRepository;
import com.example.testfinproject.dao.entity.TransactionsEntity;
import com.example.testfinproject.exceptions.NotFoundException;
import com.example.testfinproject.service.dto.TransactionsDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsServiceImplTest {

    @Mock
    private TransactionsRepository transactionsRepository;

    private TransactionsServiceImpl transactionsService;

    @Before
    public void setUp() {
        transactionsService = new TransactionsServiceImpl(transactionsRepository);
    }

    @Test
    public void save_ok() {

        TransactionsDto savedDto = transactionsService.save(new TransactionsDto());

        verify(transactionsRepository, times(1)).save(any(TransactionsEntity.class));
        assertNotNull(savedDto);
        assertNotNull(savedDto.getCreateDttm());
    }

    @Test
    public void findByUserLogin_ok() {

        doReturn(List.of(new TransactionsEntity())).when(transactionsRepository).findByPersonLogin("test");

        List<TransactionsDto> transactionsDtoList = transactionsService.findByUserLogin("test");

        verify(transactionsRepository, times(1)).findByPersonLogin("test");
        assertFalse(transactionsDtoList.isEmpty());
    }

    @Test(expected = NotFoundException.class)
    public void findByUserLogin_listIsEmpty() {

        doReturn(List.of()).when(transactionsRepository).findByPersonLogin("test");

        transactionsService.findByUserLogin("test");
    }
}