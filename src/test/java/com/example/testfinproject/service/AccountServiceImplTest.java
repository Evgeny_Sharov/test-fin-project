package com.example.testfinproject.service;

import com.example.testfinproject.dao.AccountRepository;
import com.example.testfinproject.dao.entity.AccountEntity;
import com.example.testfinproject.dao.entity.ModelForDisplayingAllAccountsEntity;
import com.example.testfinproject.exceptions.IllegalArgumentException;
import com.example.testfinproject.exceptions.NotFoundException;
import com.example.testfinproject.service.api.PersonService;
import com.example.testfinproject.service.api.TransactionsService;
import com.example.testfinproject.service.dto.AccountDto;
import com.example.testfinproject.service.dto.DepositMoneyDto;
import com.example.testfinproject.service.dto.ModelForDisplayingAllAccountsDto;
import com.example.testfinproject.service.dto.PersonDto;
import com.example.testfinproject.service.dto.TransactionsDto;
import com.example.testfinproject.service.dto.TransferMoneyDto;
import com.example.testfinproject.service.dto.WithdrawMoneyDto;
import com.example.testfinproject.utils.api.DigestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    @Mock
    private DigestService digestService;

    @Mock
    private PersonService personService;

    @Mock
    private TransactionsService transactionsService;

    @Mock
    private AccountRepository accountRepository;

    private AccountServiceImpl accountService;

    @Before
    public void setUp() {
        accountService = new AccountServiceImpl(digestService, personService, transactionsService, accountRepository);
    }

    @Test
    public void findAll() {

        doReturn(List.of(new ModelForDisplayingAllAccountsEntity())).when(accountRepository).findAll();

        List<ModelForDisplayingAllAccountsDto> accountsAll = accountService.findAll();

        verify(accountRepository, times(1)).findAll();
        assertFalse(accountsAll.isEmpty());
    }

    @Test
    public void save_ok() {
        PersonDto personDto = new PersonDto();
        personDto.setPersonId(1L);
        personDto.setLogin("test");

        doReturn(personDto).when(personService).findByLogin("test");

        AccountDto accountDto = new AccountDto();
        accountDto.setPersonDto(personDto);
        String accountNo = accountService.save(accountDto);

        verify(personService, times(1)).findByLogin("test");
        verify(accountRepository, times(1)).save(any(AccountEntity.class));
        assertNotNull(accountNo);
    }

    @Test(expected = NotFoundException.class)
    public void save_personNotFound() {

        doThrow(NotFoundException.class).when(personService).findByLogin("test");

        PersonDto personDto = new PersonDto();
        personDto.setPersonId(1L);
        personDto.setLogin("test");

        AccountDto accountDto = new AccountDto();
        accountDto.setPersonDto(personDto);

        String accountNo = accountService.save(accountDto);
    }

    @Test
    public void withdrawMoney() {
        AccountEntity selectedAccount = new AccountEntity();
        selectedAccount.setPinCode("1111");
        selectedAccount.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto withdrawMoneyDto = new WithdrawMoneyDto();
        withdrawMoneyDto.setAccountNo(123L);
        withdrawMoneyDto.setPinCode("1111");
        withdrawMoneyDto.setPrice(new BigDecimal("500"));

        doReturn(Optional.of(selectedAccount)).when(accountRepository).findByAccountNo(withdrawMoneyDto.getAccountNo());
        doReturn(selectedAccount.getPinCode()).when(digestService).hash(withdrawMoneyDto.getPinCode());

        String balanceAfterWithdraw = accountService.withdrawMoney(withdrawMoneyDto);

        verify(accountRepository, times(1)).findByAccountNo(123L);
        verify(digestService, times(1)).hash(withdrawMoneyDto.getPinCode());
        verify(accountRepository, times(1)).updateBalance(any(AccountEntity.class));
        verify(transactionsService, times(1)).save(any(TransactionsDto.class));
        assertNotNull(balanceAfterWithdraw);
        assertEquals(balanceAfterWithdraw, new BigDecimal("500").toString());
    }

    @Test(expected = NotFoundException.class)
    public void withdrawMoney_accountNotFound() {
        WithdrawMoneyDto dto = new WithdrawMoneyDto();
        dto.setAccountNo(123L);

        doReturn(Optional.empty()).when(accountRepository).findByAccountNo(dto.getAccountNo());

        String balance = accountService.withdrawMoney(dto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withdrawMoney_priceForWithdrawalWrong() {
        AccountEntity entity = new AccountEntity();
        entity.setPinCode("1111");
        entity.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto dto = new WithdrawMoneyDto();
        dto.setAccountNo(123L);
        dto.setPinCode("1111");
        dto.setPrice(new BigDecimal("5000"));

        doReturn(Optional.of(entity)).when(accountRepository).findByAccountNo(dto.getAccountNo());
        doReturn("1111").when(digestService).hash(dto.getPinCode());

        String balance = accountService.withdrawMoney(dto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withdrawMoney_pinCodeWrong() {
        AccountEntity entity = new AccountEntity();
        entity.setPinCode("1111");
        entity.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto dto = new WithdrawMoneyDto();
        dto.setAccountNo(123L);
        dto.setPinCode("1111");
        dto.setPrice(new BigDecimal("500"));

        doReturn(Optional.of(entity)).when(accountRepository).findByAccountNo(dto.getAccountNo());
        doReturn("2222").when(digestService).hash(dto.getPinCode());

        String balance = accountService.withdrawMoney(dto);
    }

    @Test
    public void depositMoney() {
        AccountEntity selectedAccount = new AccountEntity();
        selectedAccount.setPinCode("1111");
        selectedAccount.setBalance(new BigDecimal("1000"));

        DepositMoneyDto depositAccount = new DepositMoneyDto();
        depositAccount.setAccountNo(123L);
        depositAccount.setPrice(new BigDecimal("500"));

        doReturn(Optional.of(selectedAccount)).when(accountRepository).findByAccountNo(depositAccount.getAccountNo());

        String balanceAfterDeposit = accountService.depositMoney(depositAccount);

        verify(accountRepository, times(1)).findByAccountNo(depositAccount.getAccountNo());
        verify(accountRepository, times(1)).updateBalance(any(AccountEntity.class));
        verify(transactionsService, times(1)).save(any(TransactionsDto.class));
        assertNotNull(balanceAfterDeposit);
        assertEquals(balanceAfterDeposit, new BigDecimal("1500").toString());
    }

    @Test(expected = NotFoundException.class)
    public void depositMoney_accountNotFound() {
        DepositMoneyDto dto = new DepositMoneyDto();
        dto.setAccountNo(123L);

        doReturn(Optional.empty()).when(accountRepository).findByAccountNo(anyLong());

        String balance = accountService.depositMoney(dto);
    }

    @Test
    public void transferMoney() {
        BigDecimal price = new BigDecimal("500");
        Long accountNoFrom = 123L;
        Long accountNoTo = 555L;
        String pinCode = "1111";

        AccountEntity accountForWithdraw = new AccountEntity();
        accountForWithdraw.setPinCode("1111");
        accountForWithdraw.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto withdrawMoneyDto = new WithdrawMoneyDto();
        withdrawMoneyDto.setAccountNo(accountNoFrom);
        withdrawMoneyDto.setPinCode(pinCode);
        withdrawMoneyDto.setPrice(price);

        doReturn(Optional.of(accountForWithdraw)).when(accountRepository).findByAccountNo(withdrawMoneyDto.getAccountNo());
        doReturn(accountForWithdraw.getPinCode()).when(digestService).hash(withdrawMoneyDto.getPinCode());

        AccountEntity accountForDeposit = new AccountEntity();

        accountForDeposit.setBalance(new BigDecimal("5000"));

        DepositMoneyDto depositMoneyDto = new DepositMoneyDto();
        depositMoneyDto.setAccountNo(accountNoTo);
        depositMoneyDto.setPrice(price);

        doReturn(Optional.of(accountForDeposit)).when(accountRepository).findByAccountNo(depositMoneyDto.getAccountNo());

        TransferMoneyDto transferMoneyDto = new TransferMoneyDto();
        transferMoneyDto.setPinCode(pinCode);
        transferMoneyDto.setAccountNoFrom(accountNoFrom);
        transferMoneyDto.setAccountNoTo(accountNoTo);
        transferMoneyDto.setPrice(price);

        String balanceAfterWithdraw = accountService.transferMoney(transferMoneyDto);

        verify(accountRepository, times(1)).findByAccountNo(withdrawMoneyDto.getAccountNo());
        verify(accountRepository, times(1)).findByAccountNo(depositMoneyDto.getAccountNo());
        verify(digestService, times(1)).hash(withdrawMoneyDto.getPinCode());
        verify(accountRepository, times(2)).updateBalance(any(AccountEntity.class));
        verify(transactionsService, times(2)).save(any(TransactionsDto.class));

        assertNotNull(balanceAfterWithdraw);
        assertEquals(balanceAfterWithdraw, new BigDecimal("500").toString());
    }

    @Test(expected = NotFoundException.class)
    public void transferMoney_accountFoDepositNotFound() {
        BigDecimal price = new BigDecimal("500");
        Long accountNoFrom = 123L;
        Long accountNoTo = 555L;
        String pinCode = "1111";

        AccountEntity accountForWithdraw = new AccountEntity();
        accountForWithdraw.setPinCode("1111");
        accountForWithdraw.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto withdrawMoneyDto = new WithdrawMoneyDto();
        withdrawMoneyDto.setAccountNo(accountNoFrom);
        withdrawMoneyDto.setPinCode(pinCode);
        withdrawMoneyDto.setPrice(price);

        doReturn(Optional.of(accountForWithdraw)).when(accountRepository).findByAccountNo(withdrawMoneyDto.getAccountNo());
        doReturn(accountForWithdraw.getPinCode()).when(digestService).hash(withdrawMoneyDto.getPinCode());

        AccountEntity accountForDeposit = new AccountEntity();

        accountForDeposit.setBalance(new BigDecimal("5000"));

        DepositMoneyDto depositMoneyDto = new DepositMoneyDto();
        depositMoneyDto.setAccountNo(accountNoTo);
        depositMoneyDto.setPrice(price);

        doReturn(Optional.empty()).when(accountRepository).findByAccountNo(depositMoneyDto.getAccountNo());

        TransferMoneyDto transferMoneyDto = new TransferMoneyDto();
        transferMoneyDto.setPinCode(pinCode);
        transferMoneyDto.setAccountNoFrom(accountNoFrom);
        transferMoneyDto.setAccountNoTo(accountNoTo);
        transferMoneyDto.setPrice(price);

        String balanceAfterWithdraw = accountService.transferMoney(transferMoneyDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transferMoney_pinCodeForAccountWithdrawWrong() {
        BigDecimal price = new BigDecimal("5000");
        Long accountNoFrom = 123L;
        Long accountNoTo = 555L;
        String pinCode = "1111";

        AccountEntity accountForWithdraw = new AccountEntity();
        accountForWithdraw.setPinCode("1111");
        accountForWithdraw.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto withdrawMoneyDto = new WithdrawMoneyDto();
        withdrawMoneyDto.setAccountNo(accountNoFrom);
        withdrawMoneyDto.setPinCode(pinCode);
        withdrawMoneyDto.setPrice(price);

        doReturn(Optional.of(accountForWithdraw)).when(accountRepository).findByAccountNo(withdrawMoneyDto.getAccountNo());
        doReturn("2222").when(digestService).hash(withdrawMoneyDto.getPinCode());

        TransferMoneyDto transferMoneyDto = new TransferMoneyDto();
        transferMoneyDto.setPinCode(pinCode);
        transferMoneyDto.setAccountNoFrom(accountNoFrom);
        transferMoneyDto.setAccountNoTo(accountNoTo);
        transferMoneyDto.setPrice(price);

        String balanceAfterWithdraw = accountService.transferMoney(transferMoneyDto);
    }

    @Test(expected = NotFoundException.class)
    public void transferMoney_accountWithdrawNotFound() {

        doReturn(Optional.empty()).when(accountRepository).findByAccountNo(123L);

        TransferMoneyDto transferMoneyDto = new TransferMoneyDto();
        transferMoneyDto.setAccountNoFrom(123L);
        transferMoneyDto.setAccountNoTo(555L);

        String balanceAfterWithdraw = accountService.transferMoney(transferMoneyDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transferMoney_priceForWithdrawalWrong() {
        BigDecimal price = new BigDecimal("5000");
        Long accountNoFrom = 123L;
        Long accountNoTo = 555L;
        String pinCode = "1111";

        AccountEntity accountForWithdraw = new AccountEntity();
        accountForWithdraw.setPinCode("1111");
        accountForWithdraw.setBalance(new BigDecimal("1000"));

        WithdrawMoneyDto withdrawMoneyDto = new WithdrawMoneyDto();
        withdrawMoneyDto.setAccountNo(accountNoFrom);
        withdrawMoneyDto.setPinCode(pinCode);
        withdrawMoneyDto.setPrice(price);

        doReturn(Optional.of(accountForWithdraw)).when(accountRepository).findByAccountNo(withdrawMoneyDto.getAccountNo());
        doReturn(accountForWithdraw.getPinCode()).when(digestService).hash(withdrawMoneyDto.getPinCode());

        TransferMoneyDto transferMoneyDto = new TransferMoneyDto();
        transferMoneyDto.setPinCode(pinCode);
        transferMoneyDto.setAccountNoFrom(accountNoFrom);
        transferMoneyDto.setAccountNoTo(accountNoTo);
        transferMoneyDto.setPrice(price);

        String balanceAfterWithdraw = accountService.transferMoney(transferMoneyDto);
    }

    @Test
    public void findByAccountNo() {

        doReturn(Optional.of(new AccountEntity())).when(accountRepository).findByAccountNo(123L);

        AccountDto byAccountNo = accountService.findByAccountNo(123L);

        verify(accountRepository, times(1)).findByAccountNo(123L);
        assertNotNull(byAccountNo);
    }

    @Test(expected = NotFoundException.class)
    public void findByAccountNo_notFound() {

        doReturn(Optional.empty()).when(accountRepository).findByAccountNo(123L);

        AccountDto byAccountNo = accountService.findByAccountNo(123L);
    }

    @Test
    public void updateBalance() {
        AccountDto dto = new AccountDto();
        dto.setAccountNo(123L);
        dto.setBalance(new BigDecimal("500"));

        AccountEntity entity = new AccountEntity();
        entity.setAccountNo(123L);
        entity.setBalance(new BigDecimal("500"));

        accountService.updateBalance(dto);

        verify(accountRepository, times(1)).updateBalance(entity);
    }
}